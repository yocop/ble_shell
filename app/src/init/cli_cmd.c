/*
 * Copyright (C) 2019-2020 Alibaba Group Holding Limited
 */


#include <aos/aos.h>
#include <aos/cli_cmd.h>
#include <aos/cli.h>

void board_cli_init()
{
    cli_service_init();

    cli_reg_cmd_help();

    cli_reg_cmd_ps();

    cli_reg_cmd_sysinfo();

    cli_reg_cmd_free();

    cli_reg_cmd_kvtool();

    cli_reg_cmd_ble();
}
